﻿#include "PassportService.h"

#include <QJsonDocument>
#include <QJsonObject>
#include <QString>

namespace ok::backend {

using namespace session;

PassportService::PassportService(const QString& base, QObject *parent)
    : BaseService(base, parent) {}

PassportService::~PassportService() {}

bool PassportService::getAccount(const QString &account,
                                 Fn<void(Res<SysAccount> &)> fn) {
  QString url = _baseUrl + "/api/open/passport/account/" + account;
  http->getJSON(
      QUrl(url),
      // success
      [=](QJsonDocument doc) {
        Res<SysAccount> res(doc);
        fn(res);
      },
      // error
      [=](QString err) {
        qWarning()<<"error"<<err;
      });
  return true;
}

} // namespace ok::backend
