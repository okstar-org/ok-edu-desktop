#include "OkCloudService.h"

#include <QObject>

namespace ok::backend {

OkCloudService::OkCloudService(QObject *parent)
    : BaseService(BACKEND_CLOUD_URL, parent) {}

OkCloudService::~OkCloudService() {}

bool OkCloudService::GetFederalInfo(Fn<void(Res<FederalInfo> &)> fn, Fn<void(QString)> err) {
  QString url = _baseUrl + "/federal/.well-known/info.json";
  return http->getJSON(
      QUrl(url),
      // success
      [=](QJsonDocument doc) {
        Res<FederalInfo> res(doc);
        fn(res);
      },
      err);
}

} // namespace ok::backend
