#include "BookedService.h"
#include <QObject>


namespace ok::backend {

BookedService::BookedService(QObject *parent) : BaseService(""/*BACKEND_STACK_URL*/, parent) {}

BookedService::~BookedService() {}

bool BookedService::requestCurrent(Fn<void(BookedInfo)> callback) {
  QString url(_baseUrl + "/booked/current");
  return requestBooked(url, callback);
}

bool BookedService::requestNext(Fn<void(BookedInfo)> callback) {
  QString url(_baseUrl + "/booked/next.do");
  return requestBooked(url, callback);
}

bool BookedService::requestDowncount(Fn<void(DowncountInfo)> callback) {
  return false;
}

bool BookedService::requestBooked(QString &url, Fn<void(BookedInfo)> callback) {
  QMap<QString, QString> params;
  return false;
}

QString BookedService::baseUrl() { return this->_baseUrl; }
} // namespace ok::backend