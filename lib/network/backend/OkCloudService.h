﻿#pragma once

#include <QObject>

#include "base/jsons.h"
#include <lib/network/backend/BaseService.h>

namespace ok::backend {

class State {
public:
  QString no;
  QString name;
  QString xmppHost;
  QString stackUrl;
};

class FederalInfo {
public:
  FederalInfo(const QJsonObject &data) {
    /**
     * {
"states": [
  {
    "no": "1001",
    "name": "OkStar开源社区",
    "xmppHost": "meet.okstar.org.cn"
  },
  {
    "no": "1002",
    "name": "船山信息",
    "xmppHost": "meet.chuanshaninfo.com"
  }
]
}
     */

    QJsonArray arr = data.value("states").toArray();
    for (auto && item : arr) {
      const QJsonObject &object = item.toObject();
      State state;
      state.no = object.value("no").toString();
      state.name = object.value("name").toString();
      state.xmppHost = object.value("xmppHost").toString();
      state.stackUrl = object.value("stackUrl").toString();
      states.push_back(state);
    }
  }
  QList<State> states;
};

class OkCloudService : public BaseService {
  Q_OBJECT

public:
  OkCloudService(QObject *parent = nullptr);
  ~OkCloudService();

  bool GetFederalInfo(Fn<void(Res<FederalInfo> &)> fn, Fn<void(QString)> err);
};
} // namespace ok::backend
