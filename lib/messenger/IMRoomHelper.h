#pragma once

#include <string>
#include <map>
#include <QString>
#include "messenger.h"
#include <gloox/mucroom.h>

namespace lib {
namespace messenger {

using namespace gloox;

struct IMRoomInfo {
  MUCRoom *room;

  /**
   * 显示项
   * muc#roominfo_<field name>
   * 参考：
   * https://xmpp.org/extensions/xep-0045.html#registrar-formtype-roominfo
   */
   //名称 muc#roominfo_roomname
  std::string name;
  //muc#roominfo_description
  std::string description;
  //muc#roominfo_subject
  std::string subject;
  //x-muc#roominfo_creationdate
  std::string creationdate;
  //人数 muc#roominfo_occupants
  int occupants;
  GroupInfo info;



  /**
   * 房间待修改项
   * 1、修改项放入该字段；
   * 2、请求`room->requestRoomConfig()`获取服务器房间所有配置;
   * 3、服务器返回到`handleMUCConfigForm`处理，保存即可；
   *
   * muc#roomconfig_<field name>
   * 参考
   * https://xmpp.org/extensions/xep-0045.html#registrar-formtype-owner
   */
  std::map<std::string, std::string>  changes;
};



} // namespace IM
} // namespace lib
